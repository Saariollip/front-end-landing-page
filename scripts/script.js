
window.onload=function(){
  makeRSSFeed()

  /* Set image width according to device viewport */

}

/* Loading Google Feed API */
google.load('feeds', '1');

function makeRSSFeed(){

  var feedSource = "http://www.lianatech.com/news/all-news.rss";
  var numEntries = 3;
  var targetDiv = "feed";



  var feedPointer = new google.feeds.Feed(feedSource);
  feedPointer.setNumEntries(numEntries);
  feedPointer.load(populateRSSFeed);

}
google.setOnLoadCallback(makeRSSFeed);



/* Making navbar fixed if scrolling*/
function makeNavbarFixed(viewportWidth, topbarHeight){
  $("#navbar-background").addClass("navbar-fixed-top");
  $('.jumbotron-content').css('margin-top',  '22.5rem');
  $('.jumbotron').css('margin-bottom', topbarHeight + 'px');
  $("#navbar-background").css('background-color', 'rgb(255,255,255)');

}
function makeNavbarStatic(){
  $("#navbar-background").removeClass("navbar-fixed-top");
  $('.jumbotron').css('margin-bottom', '0');
  $("#navbar-background").css('background-color', 'rgba(255,255,255, 0.2)');
  $('.jumbotron-content').css('margin-top',  '15rem');
}
/* Navbar scrolling */
$(window).scroll(function(e){

    /* Using parallax only in desktop viewport because many mobile devices does not support it well */

    var viewportWidth = $( window ).width();

    if (viewportWidth > 768){
        parallax();
    }


    var topbarHeight = $('#navbar-background').height();

    if (window.pageYOffset > topbarHeight)
    {
       if (viewportWidth > 768){
          makeNavbarFixed(viewportWidth, topbarHeight);
       }
    }

    /* Making topbar visible again when scrolling to the top */
    else if (window.pageYOffset <= topbarHeight) {
      makeNavbarStatic();
    }

});

/* Parallax implementation for the jumbotron */

function parallax(){
  var jumboHeight = $('.jumbotron').height();
  var scrolled = $(window).scrollTop();
  $('.bg').css('height', (jumboHeight-scrolled) + 'px');
}


/* Rendering the RSS feed*/
function populateRSSFeed(result) {
    var divID = "first-feed";
    var firstTargetDiv = document.getElementById(divID);

    var divID = "second-feed";
    var secondTargetDiv = document.getElementById(divID);

    var divID = "third-feed";
    var thirdTargetDiv = document.getElementById(divID);


        if (!result.error) {

            var feeds = result.feed.entries;
            var output = "";

            for (var i = 0; i < feeds.length; i++) {
              var entryDate = new Date(feeds[i].publishedDate);  // Getting date
              var entryDateStr = '' + entryDate.getDay() + "." + entryDate.getMonth() + "." + entryDate.getFullYear(); //Formatting date string
              output = "<p>" + entryDateStr + "</p>";
              output += "<h4><a href=" + feeds[i].link + ">" + feeds[i].title + "</a></h4>";



              if (i == 0)
              {
                firstTargetDiv.innerHTML=output;
              }
              else if (i == 1)
              {
                secondTargetDiv.innerHTML=output;
              }
              else
              {
              thirdTargetDiv.innerHTML=output;

              }
            }

        }
        else
            alert('Error fetching feeds!');
}

/* Displaying success message when email is sent */
  $('#subscribe-form').submit(function(ev){
    ev.preventDefault();

    $("#success-message").addClass("alert alert-success");
    $("#success-message").append("<strong> Thank you! </strong> your email has been sent succesfully.");

  });
